#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D			#
# Emory University, Dept. of Human Genetics	#
# ashok.reddy.dinasarapu@emory.edu		#
# Date: 08/11/2018				#
#################################################

# 1. Rarefy table
# 2. Alpha and Beta diversity

# makes the job dependent on the previous job completion
# qsub -hold_jid <job id> ../script/qiime2.2017.8_step4b.sh

echo "Start - `date`" 
#$ -N diversity.20_245.9
#$ -q b.q
##$ -l h_rt=220:00:00
#$ -pe smp 9
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

# number of threads (#$ -pe smp 9)
CORES=9
EXPERIMENT=VFE11371

SUBJECT_NAME=subject_9
SAMPLING_DEPTH=38871


# load module
source activate qiime2-2018.8

# create a TMP directory
export TMPDIR=/scratch
if [ -e /bin/mktemp ]; then
  TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
  TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
  echo “Error. Cannot find mktemp to create tmp directory”
  exit
fi

## PROJECT DIR
PROJ_DIR=$HOME/microbiome/$EXPERIMENT
QIIME2_DIR=$PROJ_DIR/qiime2_2018_8

## INPUT DIR
# path to filtered table.qza
DATA_DIR1=$QIIME2_DIR/data_filtered/dada2/trim_20_245/$SUBJECT_NAME
DATA_DIR2=$QIIME2_DIR/data_rarefaction/dada2/trim_20_245/$SUBJECT_NAME

## OUTPUT DIR
# local dir for final results
OUT_DIR=$QIIME2_DIR/data_diversity/dada2/trim_20_245/$SUBJECT_NAME

# metadata file name
META_FILE=${EXPERIMENT}.txt
# metadata column name
METADATA_CATEGORY="SubjectGroup"

# copy metadata, feature table and sequences
rsync -av $PROJ_DIR/$META_FILE $TMP_DIR/meta_file.txt
rsync -av $DATA_DIR1/${SUBJECT_NAME}_subset_table.qza $TMP_DIR/filtered_table.qza
rsync -av $DATA_DIR2/phylogeny/rooted_tree.qza $TMP_DIR/

RAREFY_TABLE=true
ALPHA_DIVERSITY_ANALYSIS=true
BETA_DIVERSITY_ANALYSIS=true

ls -la $TMP_DIR/

# The diversity in a single sample (alpha diversity) is commonly measured using metrics such as 
# the Shannon index and the Chao1 estimator, while the variation between pairs of samples (beta diversity) 
# is measured using metrics such as the Jaccard distance or Bray-Curtis dissimilarity. 

# Many such metrics, including Shannon, Chao1, Jaccard and Bray-Curtis, are calculated from OTU frequencies.

#===========================#
# 1. Rarefy a feature table #
#===========================#

if [ $RAREFY_TABLE = 'true' ]; then

  # Each column is subsampled to even depth without replacement (hypergeometric model)
  # Correction for unequal library sizes is applied
  # It is a single random draw.

  # SAMPLING_DEPTH=1119
  qiime feature-table rarefy \
   --i-table $TMP_DIR/filtered_table.qza \
   --p-sampling-depth $SAMPLING_DEPTH \
   --o-rarefied-table $TMP_DIR/rarefied_table.qza

fi

#====================#
# 2. Alpha Diversity #
#====================#

# Alpha and beta diversity analyses, or measures of diversity within and between samples, respectively 
# (a.k.a., “how similar are my samples?”)

# Computes a user-specified alpha diversity metric for all samples in a feature table
# At the moment you can only supply one metric at a time 

if [ $ALPHA_DIVERSITY_ANALYSIS = 'true' ]; then

  /bin/mkdir -p $TMP_DIR/alpha_diversity

  ## ALPHA_NON_PHYLOGENETIC (shannon, chao1, ...)

  SHANNON=true

  if [ $SHANNON == 'true' ]; then

    qiime diversity alpha \
     --i-table $TMP_DIR/rarefied_table.qza \
     --p-metric shannon \
     --output-dir $TMP_DIR/alpha_diversity/shannon

    qiime tools export \
     --output-path $TMP_DIR/alpha_diversity/shannon/export \
     --input-path $TMP_DIR/alpha_diversity/shannon/alpha_diversity.qza

    # Visually and statistically compare groups of alpha diversity values
    qiime diversity alpha-group-significance \
     --i-alpha-diversity $TMP_DIR/alpha_diversity/shannon/alpha_diversity.qza \
     --m-metadata-file $TMP_DIR/meta_file.txt \
     --o-visualization $TMP_DIR/alpha_diversity/shannon/alpha_group_significance.qzv

  fi

  # Determine whether numeric sample metadata category is correlated with alpha diversity
  # The alpha-correlation method correlates an alpha diversity metric with all numeric values in the input mapping file
  
  ALPHA_CORRELATION=true

  if [ $ALPHA_CORRELATION == 'true' ]; then

    qiime diversity alpha-correlation \
     --i-alpha-diversity $TMP_DIR/alpha_diversity/shannon/alpha_diversity.qza \
     --m-metadata-file $TMP_DIR/meta_file.txt \
     --o-visualization $TMP_DIR/alpha_diversity/shannon/alpha_correlation.qzv

  fi

  # metric: chao1
  # Some alpha diversity metrics, including Chao1 and Robbins, explicitly use singleton counts or singleton frequencies 
  # in their formulas. If singleton unique reads or singleton OTUs are discarded, then these calculations are obviously invalid.
  
  CHAO1=true
  
  if [ $CHAO1 == 'true' ]; then

    qiime diversity alpha \
     --i-table $TMP_DIR/rarefied_table.qza \
     --p-metric chao1 \
     --output-dir $TMP_DIR/alpha_diversity/chao1

    qiime tools export \
     --output-path $TMP_DIR/alpha_diversity/chao1/export \
     --input-path $TMP_DIR/alpha_diversity/chao1/alpha_diversity.qza

    qiime diversity alpha-group-significance \
     --i-alpha-diversity $TMP_DIR/alpha_diversity/chao1/alpha_diversity.qza \
     --m-metadata-file $TMP_DIR/meta_file.txt \
     --o-visualization $TMP_DIR/alpha_diversity/chao1/alpha_group_significance.qzv

  fi

  ## ALPHA_PHYLOGENETIC (faith_pd, ...)
  # Computes a user-specified phylogenetic alpha diversity metric for all samples in a feature table
  # /bin/mkdir -p $TMP_DIR/alpha_diversity/faith_pd

  FAITH_PD=true

  if [ $FAITH_PD ]; then

    qiime diversity alpha-phylogenetic \
     --i-table $TMP_DIR/rarefied_table.qza \
     --i-phylogeny $TMP_DIR/rooted_tree.qza \
     --p-metric faith_pd \
     --output-dir $TMP_DIR/alpha_diversity/faith_pd

    qiime tools export \
     --output-path $TMP_DIR/alpha_diversity/faith_pd/export \
     --input-path $TMP_DIR/alpha_diversity/faith_pd/alpha_diversity.qza

    qiime diversity alpha-group-significance \
     --i-alpha-diversity $TMP_DIR/alpha_diversity/faith_pd/alpha_diversity.qza \
     --m-metadata-file $TMP_DIR/meta_file.txt \
     --o-visualization $TMP_DIR/alpha_diversity/faith_pd/alpha_group_significance.qzv

  fi

fi

#===================#
# 3. Beta Diversity #
#===================#

# Computes a user-specified beta diversity metric for all pairs of samples in a feature table.
# All beta diversity metrics use OTU frequencies or presence / absence, neither of which can be reliably determined from amplicon reads.

if [ $BETA_DIVERSITY_ANALYSIS = 'true' ]; then

  /bin/mkdir -p $TMP_DIR/beta_diversity
   
  ## BETA_NON_PHYLOGENETIC_DIVERSITY (braycurtis, jaccard, ...)
  # metric: braycurtis

  BRAYCURTIS=true

  if [ $BRAYCURTIS == 'true' ]; then

    qiime diversity beta \
     --i-table $TMP_DIR/rarefied_table.qza \
     --p-metric braycurtis \
     --output-dir $TMP_DIR/beta_diversity/braycurtis

    # Determine whether groups of samples are significantly different from one
    # another using a permutation-based statistical test
    qiime diversity beta-group-significance \
     --i-distance-matrix $TMP_DIR/beta_diversity/braycurtis/distance_matrix.qza \
     --m-metadata-file $TMP_DIR/meta_file.txt \
     --m-metadata-column $METADATA_CATEGORY \
     --p-permutations 999 \
     --p-method permanova \
     --p-pairwise \
     --o-visualization $TMP_DIR/beta_diversity/braycurtis/beta_group_significance.qzv

    # Apply principal coordinate analysis
    # The basic test of how well broad differences in microbial sample composition are detected, as assessed by clustering analysis, 
    qiime diversity pcoa \
     --i-distance-matrix $TMP_DIR/beta_diversity/braycurtis/distance_matrix.qza \
     --o-pcoa $TMP_DIR/beta_diversity/braycurtis/braycurtis_pcoa.qza

    # Generate visualization of your ordination
    qiime emperor plot \
     --i-pcoa $TMP_DIR/beta_diversity/braycurtis/braycurtis_pcoa.qza \
     --m-metadata-file $TMP_DIR/meta_file.txt \
     --o-visualization $TMP_DIR/beta_diversity/braycurtis/braycurtis_pcoa_emperor.qzv

    qiime tools export \
     --output-path $TMP_DIR/beta_diversity/braycurtis/export \
     --input-path $TMP_DIR/beta_diversity/braycurtis/braycurtis_pcoa.qza
  fi

  JACCARD=true

  if [ $JACCARD == 'true' ];then

    qiime diversity beta \
     --i-table $TMP_DIR/rarefied_table.qza \
     --p-metric jaccard \
     --output-dir $TMP_DIR/beta_diversity/jaccard

    qiime diversity beta-group-significance \
     --i-distance-matrix $TMP_DIR/beta_diversity/jaccard/distance_matrix.qza \
     --m-metadata-file $TMP_DIR/meta_file.txt \
     --m-metadata-column $METADATA_CATEGORY \
     --p-permutations 999 \
     --p-method permanova \
     --p-pairwise \
     --o-visualization $TMP_DIR/beta_diversity/jaccard/beta_group_significance.qzv

    qiime diversity pcoa \
     --i-distance-matrix $TMP_DIR/beta_diversity/jaccard/distance_matrix.qza \
     --o-pcoa $TMP_DIR/beta_diversity/jaccard/jaccard_pcoa.qza

    qiime emperor plot \
     --i-pcoa $TMP_DIR/beta_diversity/jaccard/jaccard_pcoa.qza \
     --m-metadata-file $TMP_DIR/meta_file.txt \
     --o-visualization $TMP_DIR/beta_diversity/jaccard/jaccard_pcoa_emperor.qzv

    qiime tools export \
     --output-path $TMP_DIR/beta_diversity/jaccard/export \
     --input-path $TMP_DIR/beta_diversity/jaccard/jaccard_pcoa.qza
  fi

  ## BETA_PHYLOGENETIC_DIVERSITY (unweighted_unifrac, weighted_unifrac, ...)
  
  UNWAIGHTED_UNIFRAC=true

  if [ $UNWAIGHTED_UNIFRAC ]; then

    qiime diversity beta-phylogenetic \
     --i-table $TMP_DIR/rarefied_table.qza \
     --i-phylogeny $TMP_DIR/rooted_tree.qza \
     --p-metric unweighted_unifrac \
     --output-dir $TMP_DIR/beta_diversity/unweighted_unifrac

    qiime diversity beta-group-significance \
     --i-distance-matrix $TMP_DIR/beta_diversity/unweighted_unifrac/distance_matrix.qza \
     --m-metadata-file $TMP_DIR/meta_file.txt \
     --m-metadata-column $METADATA_CATEGORY \
     --p-permutations 999 \
     --p-method permanova \
     --p-pairwise \
     --o-visualization $TMP_DIR/beta_diversity/unweighted_unifrac/beta_group_significance.qzv

    qiime diversity pcoa \
     --i-distance-matrix $TMP_DIR/beta_diversity/unweighted_unifrac/distance_matrix.qza \
     --o-pcoa $TMP_DIR/beta_diversity/unweighted_unifrac/unweighted_unifrac_pcoa.qza
 
    qiime emperor plot \
     --i-pcoa $TMP_DIR/beta_diversity/unweighted_unifrac/unweighted_unifrac_pcoa.qza \
     --m-metadata-file $TMP_DIR/meta_file.txt \
     --o-visualization $TMP_DIR/beta_diversity/unweighted_unifrac/unweighted_unifrac_pcoa_emperor.qzv
 
    qiime tools export \
     --output-path $TMP_DIR/beta_diversity/unweighted_unifrac/export \
     --input-path $TMP_DIR/beta_diversity/unweighted_unifrac/unweighted_unifrac_pcoa.qza
  fi

fi

source deactivate qiime2-2018.8

/bin/rm $TMP_DIR/filtered_table.qza
/bin/rm $TMP_DIR/rooted_tree.qza

if [ ! -d $OUT_DIR ]; then
 mkdir -p $OUT_DIR
fi

rsync -av $TMP_DIR/* $OUT_DIR
/bin/rm -rf $TMP_DIR

echo "Finish - `date`"
