#!/bin/sh

echo "Start - `date`" 
#$ -N import.SDE11844
#$ -q b.q
#$ -pe smp 1
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

echo ---------------------------------
echo -e `cat $PE_HOSTFILE`
echo ---------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ---------------------------------

source activate qiime2-2018.8

PROJ_DIR=$HOME/microbiome/SDE11844
QIIME2_DIR=$PROJ_DIR/qiime2_2018_8
OUT_DIR=$QIIME2_DIR/data_imported

if [ ! -d ${OUT_DIR} ]; then
 mkdir -p ${OUT_DIR}
fi

export TMPDIR=/scratch

if [ -e /bin/mktemp ]; then
 TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

FASTQ_DIR=$PROJ_DIR/FASTQ/combined
rsync -av $FASTQ_DIR/[!Undetermined]*_R[1-2]_001.fastq.gz $TMP_DIR

ls -la $TMP_DIR/

# creates manifest.txt file 
# with header
# sample-id,absolute-filepath,direction
echo -e "sample-id,absolute-filepath,direction" >> $TMP_DIR/manifest.txt

for entry in $TMP_DIR/*.fastq.gz; do

 # file name
 fname=$(basename $entry)

 # split fastq.gz file name before "_S" character (change this if necessary)
 fbname=${fname%_R*}

 # makesure to check both forward and reverse read files exist
 # fcount=`ls $TMP_DIR/ | grep $fbname$ | wc -l`
 fcount=`ls $TMP_DIR/ | xargs -n1 basename | awk 'BEGIN{FS="_"}{ print $1 }' | grep $fbname$ | wc -l`
 # echo $paired_files
 # fcount=$(echo $paired_files | wc -l)

 echo $fbname $fcount

 if [[ ${fcount} == 2 ]]; then
  # old version - Don't use underscores (_) in sample-id of manifest file 
  #fbname=`echo $fbname | sed 's/_/-/'`
  fpre="$fbname,$TMP_DIR/$fname"
  if [[ $entry == *"_R1_"* ]]; then
   echo -e "$fpre,forward" >> $TMP_DIR/manifest.txt
  fi
  if [[ $entry == *"_R2_"* ]]; then
   echo -e "$fpre,reverse" >> $TMP_DIR/manifest.txt
  fi
 fi
done

cat $TMP_DIR/manifest.txt

#=====================================================#
# Import FASTQ files to create a new QIIME 2 Artifact #
#=====================================================#
# --type		: The semantic type of the artifact that will be created upon importing
# --source-format	: The format of the data to be imported

qiime tools import \
 --type SampleData[PairedEndSequencesWithQuality] \
 --input-path $TMP_DIR/manifest.txt \
 --output-path $TMP_DIR/paired_end_data.qza \
 --input-format PairedEndFastqManifestPhred33

# Validate data in a QIIME 2 Artifact.
# --level [min|max]  Desired level of validation.

qiime tools validate \
 --level max $TMP_DIR/paired_end_data.qza > $TMP_DIR/imported_data_validation.txt

# Summarize counts per sample for all samples
# Plot positional qualitites 
# --p-n : The number of sequences that should be selected at random for quality score plots

qiime demux summarize \
 --i-data $TMP_DIR/paired_end_data.qza \
 --p-n 10000 \
 --o-visualization $TMP_DIR/imported_data_qualities.qzv

# before copying results, remove all uploaded raw data
/bin/rm $TMP_DIR/*.fastq.gz

rsync -av $TMP_DIR/* $OUT_DIR

# After copying results, remove tmp directory
/bin/rm -rf $TMP_DIR

source deactivate qiime2-2018.8

echo "Finish - `date`"
