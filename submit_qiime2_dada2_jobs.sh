#!/bin/sh

PROJ_DIR=$HOME/microbiome/VFE11371
QIIME2_DIR=$PROJ_DIR/qiime2_2018_8
DATA_DIR=$QIIME2_DIR/data_imported/BATCH2
TABLE=$DATA_DIR/paired_end_data.qza

OUT_DIR=$QIIME2_DIR/data_denoised/BATCH2

SCRIPT=$HOME/microbiome/script/qiime2.2018.8/qiime2.2018.8_step2_dada2.sh
META_FILE=$PROJ_DIR/batch2_metadata_updated.txt

if [ ! -d $OUT_DIR ]; then
  mkdir -p $OUT_DIR
fi

THREADS=10

for S in 0 20; do
 for E in 245 250 260 270 280 290 300; do
  qsub -v S=$S,E=$E,TABLE=$TABLE,META_FILE=$META_FILE,OUT=$OUT_DIR,THREADS=$THREADS \
   -N ASV${S}${E}b2 \
   -q b.q \
   -pe smp $THREADS \
   -j y \
   -cwd $SCRIPT
 done
done
