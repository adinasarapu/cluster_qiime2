# Amplicon Metagenomics

The following pipeline is intended to help our collaborators (and ourselves) in analysis of 16S rRNA sequencing data via QIIME 2 (2018.6).

## 1. Connect to a HGCC (Human Genetics Computing Cluster) linux cluster

`ssh <username>@hgcc.genetics.emory.edu`

## 2. Install (qiime2-2018.6) and activate

Installation instructions are available at [https://docs.qiime2.org](https://docs.qiime2.org/2018.6/install/)

`source activate qiime2-2018.6`

## 3. Create a metadata file

Metadata is information about your samples i.e samples as rows and variables as columns. Create a .tsv (.txt) version of your metadata spreadsheet. 

See [Metadata in QIIME 2](https://docs.qiime2.org/2018.2/tutorials/metadata/#artifacts-as-metadata) 

To inspect metadata file for correct formatting run the following command

`qiime tools inspect-metadata sample-metadata.tsv`

Create a visualization of your metadata and view using QIIME 2 viewer (.qzv is a qiime zipped visualization).

`qiime metadata tabulate --m-input-file sample-metadata.tsv --o-visualization sample-metadata.qzv`

## 4. Raw data import (use `qiime2.2018.6_step1.sh`)

a. Create a manifest file with header`#sample-id,absolute-filepath,direction`
b. Import FASTQ files to create a QIIME 2 Artifact
c. Summarize counts per sample for all samples.

## 5.  Denoise, dereplicate and remove chimeras (use `qiime2.2018.6_step2.sh`)

Denoise sequences in order to better discriminate between true sequence diversity and sequencing errors. DADA2 denoising will join paired reads.
During dereplication, the data will be condensed by collapsing together all reads that encode the same sequence, which significantly reduces later computation times.

## 6. Filter and summarize features and samples (use `qiime2.2018.6_step3.sh`)

Filter Features (and/or Samples based feature count) and subsetting of samples  based on a metadata file.
If you have multiple runs, it is recommended that you run DADA2 separately on each batch of samples and then combine.
Use  `qiime2.2018.6_merge.sh` for merging of feature-table and representative sequences.

## 7. Build a phylogenetic tree for diversity analysis (use  `qiime2.2018.6_step4a.sh`)

Many diversity analyses rely on the phylogenetic similarity between individual features. 

## 8. To rarefy a table and compute alpha and beta diversity metrics (use  `qiime2.2018.6_step4b.sh`)

To assign taxonomy to your sequences, first train your own classifier on GreenGenes/Silva databases with 97% OTUs. 

* Alpha rarefaction curves - shows the impact of rarefaction (sampling at different depths) on alpha diversity of each sample. 
* Alpha and Beta diversity - select a sample-depth value based on the alpha-rarefaction analysis.
* Create a PCoA plot to explore beta diversity metric. To do this you can use Emperor, a powerful tool for interactively exploring scatter plots. 

## 9. Assign taxonomy to each sequence and calculate differential abundance (use  `qiime2.2018.6_step5.sh`)

* To assign taxonomy to your sequences, first train your own classifier on GreenGenes database with 97% OTUs.
* Assign taxonomy - assign taxonomy to each sequence (phylum -> class -> … genus-> species).
* Differential abundance - QIIME 2 uses ANCOM to identify differentially abundant taxa.

## 10. Exporting and modifying BIOM tables e.g. adding taxonomy annotations (use `qiime2.2018.6_biom.sh`)
